mod consts;
mod crypt;
mod phash;
mod png;
mod jpeg;
mod edge_detection;
mod utils;


#[macro_use]
extern crate log;
extern crate core;

use clap::Parser;
use chrono;
use image::Rgb;
use image::{Luma, GrayImage, DynamicImage, RgbImage};
use std::env;
use std::str;
use std::string::String;
use std::time::SystemTime;


#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    process: String,
    filepath: String,
    data: Option<String>,
}

fn crtify(path: String) -> String {
    let ext = path.split( "." ).last().unwrap();
    if ext.contains( "png" ) {
        let png = png::PNG::new(path);
        png.crtify()   
    } else if ext.contains( "jpg" ) || ext.contains( "jpeg" ) {
        let jpeg = jpeg::JPEG::new(path);
        jpeg.crtify()
    } else {
        panic!("Unaccepted image type. Aborting.");
    }
}

fn edge(path: String, colour: bool) -> String {
    let mut height: u32 = 0;
    let mut width: u32 = 0;
    let image: DynamicImage;
    let mut out_path  = path.split("/").collect::<Vec<&str>>();
    let file_name = out_path.pop().unwrap();
    let output_name = format!("{}/{}_{}", out_path.join("/"), chrono::offset::Utc::now(), file_name);

    let ext = path.split( "." ).last().unwrap();
    if ext.contains( "png" ) {
        let img = png::create(path);
        height = img.height;
        width = img.width;
        image = img.image;
    } else if ext.contains( "jpg" ) || ext.contains( "jpeg" ) {
        let img = jpeg::create(path);
        height = img.height;
        width = img.width;
        image = img.image;
    } else {
        panic!("Unaccepted image type. Aborting.");
    }

    // All operations use a 2D Vec of F32 and image `.to_vec()` outputs a 1D Vec so instead we
    // create an intermediary image instance in RGB F32 and add those values to `new_img`.
    let mut new_img: Vec<Vec<f32>> = vec![vec![0.0;height as usize];width as usize];
    let temp_image = image.into_rgb32f();

    let stop_x = width as usize;
    let stop_y = height as usize;

    for x in 0..stop_x {
        for y in 0..stop_y {
            let gray = (0.2126 * temp_image.get_pixel(x as u32, y as u32)[0]) + (0.7152 * temp_image.get_pixel(x as u32, y as u32)[1]) + (0.0722 * temp_image.get_pixel(x as u32, y as u32)[2]);
            new_img[x][y] = gray;
        }
    }

    let edge = edge_detection::detect(new_img);

    error!("Edge detection area size: {:?} x {:?}", edge.len(), edge[0].len());
    
    if colour {
        let mut altered_image = RgbImage::new(width, height);
        for x in 0..stop_x {
            for y in 0..stop_y {
                let pix_arr = edge[x][y].clone();
                let data = [0, (pix_arr * 255.0) as u8, 0];
                altered_image.put_pixel(x as u32, y as u32, Rgb(data));
            }
        }
    
        info!("Saving file to {output_name}");
    
        match altered_image.save(&output_name) {
            Ok(..) => info!("Saved file"),
            Err(e) => error!("{e}")
        };
    } else {
        let mut altered_image = GrayImage::new(width, height);
        for x in 0..stop_x {
            for y in 0..stop_y {
                let pix_arr = edge[x][y].clone();
                let data = [(pix_arr * 255.0) as u8];
                altered_image.put_pixel(x as u32, y as u32, Luma(data));
            }
        }
    
        info!("Saving file to {output_name}");
    
        match altered_image.save(&output_name) {
            Ok(..) => info!("Saved file"),
            Err(e) => error!("{e}")
        };
    }

    return output_name
}

fn encode( path: String, data: Vec<u8> ) -> String {
    let mut encoded: String = String::new();
    let ext = path.split( "." ).last().unwrap();
    if ext.contains( "png" ) {
        info!( "File is a png. Processing..." );
        let image = png::create( path );
        encoded = image.encode( data );
    } else if ext.contains( "jpg" ) {
        info!( "File is a jpg. Processing..." );
        let image = jpeg::create( path );
        encoded = image.encode( data );
    } else {
        panic!("Unaccepted image type. Aborting.");
    }

    return encoded;
}

fn decode( path: String, secret_key: String ) -> String {
    let mut decoded: Vec::<u8> = Vec::new();

    let ext = path.split( "." ).last().unwrap();
    if ext.contains( "png" ) {
        info!( "File is a png. Processing..." );
        let image = png::create( path );
        decoded = image.decode();
    } else if ext.contains( "jpg" ) {
        info!( "File is a jpg. Processing..." );
        let image = jpeg::create( path );
        decoded = image.decode().unwrap();
    } else {
        panic!("Unaccepted image type. Aborting.");
    }

    let plain_text = crypt::decrypt( secret_key, decoded.as_slice());
    return String::from_utf8( plain_text ).unwrap();
}

fn main() {
    let start = SystemTime::now();
    env_logger::init();

    info!("Starting up");

    let secret_key = env::var( "SEC_K" ).unwrap_or( String::from( "Super_Secret_Key_To_Protect_You__" ) );

    let args: Args = Args::parse();
    let path = args.filepath.clone();
    let process = args.process;
    let mut output = String::new();
    let mut encrypted= Vec::new();
    let mut data = String::new();

    let mut start_sequence: Vec<u8> = consts::START.as_bytes().to_vec();

    error!( "{:?}", start_sequence );

    if process.as_str() == "encode" {
        data = args.data.expect( "No data found to encode. Aborting." );
        if data.len() > 0 {
            encrypted = crypt::encrypt( secret_key.clone(), data );
        }
        debug!("Found: {:?}", encrypted);
    }

    start_sequence.append( &mut encrypted );
    start_sequence.append( &mut consts::END.as_bytes().to_vec() );

    // let default_parallelism_approx = available_parallelism().unwrap().get();
    // println!("Available cores (approx.): {}", default_parallelism_approx);

    match process.as_str() {
        "edge" => output = edge( path, true ),
        "phash" => output = phash::phash( path ),
        "encode" => output = encode( path, start_sequence ),
        "decode" => output = decode( path, secret_key ),
        "crtify" => output = crtify(path),
         _ => info!( "No matched process found. Aborting." )
    }

    info!("Output: {}", output);

    let end = SystemTime::now();
    let duration = end.duration_since( start ).unwrap();
    info!("It took {} milliseconds", duration.as_millis());
}
